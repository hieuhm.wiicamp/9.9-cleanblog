// api/aboutApi.js

import axiosClient from './axiosClient';

class AboutApi {
  getAll = (params) => {
    const url = '/Posts';
    return axiosClient.get(url, { params });
  };

  getById = (id) => {
    const url = `/Posts/${id}`;
    return axiosClient.get(url);
  }
}

const aboutApi = new AboutApi();
export default aboutApi;
