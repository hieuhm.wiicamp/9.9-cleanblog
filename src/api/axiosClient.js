import axios from 'axios';

const axiosClient = axios.create({ // Get API
  baseURL: 'http://js-post-api.herokuapp.com/api', // link muốn get api
  headers: {
    'Content-type': 'application/json',
  },
});

// Làm một số việc gì đó trước trong khi gửi request
// Add a request interceptor
axiosClient.interceptors.request.use((config) => config,
  (error) => Promise.reject(error));

// Add a response interceptor
// Any status code that lie within the range of 2xx cause this function to trigger
// Do something with response data
axiosClient.interceptors.response.use((response) => response.data,
  (error) => Promise.reject(error));

export default axiosClient;
