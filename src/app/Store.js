import { configureStore } from '@reduxjs/toolkit';
// eslint-disable-next-line import/no-named-as-default
import blogSlicer from '../blog/blogSlicer';

// B4
const store = configureStore({
  reducer: {
    blog: blogSlicer,
  },
});

export default store;
