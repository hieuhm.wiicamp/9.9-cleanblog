import React, { useEffect, useState } from 'react';
// import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import aboutApi from '../api/aboutApi';
import PostBg from '../assets/img/post-bg.jpg';
// import { saveData } from '../blog/blogSlicer';

// eslint-disable-next-line react/prop-types
const PostDetail = ({ match }) => {
  // const dispatch = useAppDispatch();
  console.log(match);

  const [postItem, setPostItem] = useState({});

  useEffect(() => {
    // eslint-disable-next-line react/prop-types
    aboutApi.getById(match.params.id).then((item) => {
      setPostItem(item);
      // useDispatch(saveData());
    });
  }, []);

  return (
    <div>
      <header className="masthead" style={{ backgroundImage: `url(${PostBg})` }}>
        <div className="container position-relative px-4 px-lg-5">
          <div className="row gx-4 gx-lg-5 justify-content-center">
            <div className="col-md-10 col-lg-8 col-xl-7">
              <div className="post-heading">
                <h1>Man must explore, and this is exploration at its greatest</h1>
                <h2 className="subheading">Problems look mighty small from 150 miles up</h2>
                <span className="meta">
                  Posted by
                  <a href="#!">Start Bootstrap</a>
                  on August 24, 2021
                </span>
              </div>
            </div>
          </div>
        </div>
      </header>

      {postItem ? (
        <article className="mb-4">
          <Link
            to="/"
            style={{ width: 'fit-content' }}
            className="back__home d-block m-auto btn mt-5 mb-5 bg-success border border-light rounded"
          >
            Back home
          </Link>

          <div className="container px-4 px-lg-5">
            <div className="row gx-4 gx-lg-5 justify-content-center">
              <div className="col-md-10 col-lg-8 col-xl-7 text-center">
                <h2>
                  {postItem.title}
                </h2>
                <div>
                  <img width="100%" height="200" src={postItem.imageUrl} alt="" />
                </div>
                <p>
                  {postItem.description}
                </p>
                <p>
                  Post by
                  {postItem.author}
                </p>
              </div>
            </div>
          </div>
        </article>
      ) : ''}
    </div>
  );
};

export default PostDetail;
