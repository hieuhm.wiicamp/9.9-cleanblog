import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import HomeBg from '../assets/img/home-bg.jpg';

const Index = () => {
  // const [curentData, setCurentData] = useState([]);
  const { data } = useSelector((state) => state.blog.blogData);
  // setCurentData(data);
  // console.log(data);
  // if (curentData) console.log(curentData);
  return (
    <div>
      <header className="masthead" style={{ backgroundImage: `url(${HomeBg})` }}>
        <div className="container position-relative px-4 px-lg-5">
          <div className="row gx-4 gx-lg-5 justify-content-center">
            <div className="col-md-10 col-lg-8 col-xl-7">
              <div className="site-heading">
                <h1>Clean Blog</h1>
                <span className="subheading">A Blog Theme by Start Bootstrap</span>
              </div>
            </div>
          </div>
        </div>
      </header>

      <div className="container px-4 px-lg-5">
        <div className="row gx-4 gx-lg-5 justify-content-center">
          <div className="col-md-10 col-lg-8 col-xl-7">
            {data ? data.map((item) => (
              <div className="post-preview" key={item.id}>
                <Link to={`post/${item.id}`}>
                  <h2 className="post-title">{item.title}</h2>
                  <h3 className="post-subtitle text-truncate-3">{item.description}</h3>
                </Link>
                <p className="post-meta">
                  Posted by &nbsp;
                  <a href="#!">{item.author}</a>
                  on September 24, 2021
                </p>
              </div>
            )) : ''}
            <hr className="my-4" />
            <div className="d-flex justify-content-end mb-4"><a className="btn btn-primary text-uppercase" href="#!">Older Posts →</a></div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Index;
