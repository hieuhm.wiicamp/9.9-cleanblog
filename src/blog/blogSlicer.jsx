import { createSlice } from '@reduxjs/toolkit';

// B1
const initialState = {
  blogData: [],
  number: 0,
};

// B2
const blogSlicer = createSlice({
  name: 'blog',
  initialState,
  reducers: {
    saveData: (state, action) => {
      const newState = state;
      newState.blogData = action.payload;
      // console.log(newState.blogData);
    },
  },
});

// Action creators are generated for each case reducer function

// B3
export const { saveData } = blogSlicer.actions;

export default blogSlicer.reducer;
