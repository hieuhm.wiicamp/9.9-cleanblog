import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import aboutApi from './api/aboutApi';
import './App.css';
import { saveData } from './blog/blogSlicer';
import About from './components/About';
import Contact from './components/Contact';
import Footer from './components/Footer';
import Index from './components/Index';
import Navigation from './components/Navigation';
import NotFound from './components/NotFound';
import Post from './components/Post';
import PostDetail from './components/PostDetail';

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchAboutList = async () => {
      try {
        const params = { _page: 1, _limit: 10 };
        const reponse = await aboutApi.getAll(params);
        dispatch(saveData(reponse));
        // console.log(reponse);
      } catch (err) {
        console.log('Failed', err);
      }
    };
    fetchAboutList();
  }, []);

  return (
    <div>
      <Router>
        <Navigation />
        <Switch>
          <Route path="/" exact component={Index} />
          <Route path="/about" component={About} />
          <Route path="/contact" component={Contact} />
          <Route path="/post" exact component={Post} />
          <Route path="/post/:id" component={PostDetail} />
          <Route path="*" component={NotFound} />
        </Switch>
        <Footer />
      </Router>
    </div>
  );
};

export default App;
